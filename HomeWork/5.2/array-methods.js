//1
// const camelize = (str) => {
//     let splitStr = str.split('-')
//     let endStr = []
//     splitStr.map((word, index) => {
//         if (index == 0) {
//             endStr.push(word)
//         } else {
//             endStr.push(word[0].toUpperCase() + word.slice(1))
//         }
//     })
//     return endStr.join('')
// }

// //2
// const filterRange = (arr, a, b) => {
//     let filterArr = []
//     arr.map(num => {
//         if ((num >= a) && (num <= b)) {
//             filterArr.push(num)
//         }
//     })

//     return filterArr
// }

//3
// const filterRangeInPlace = (arr, a, b) => {
//     arr.map((num, i) => {
//         if ((num < a) || (num > b)) {
//             arr.splice(i, 1)
//         }
//     })
// }

// let arr = [5, 2, 1, -10, 8];

// arr.sort()
// arr.reverse()

// console.log(arr); // 8, 5, 2, 1, -10


// 4
// let vasya = {
//     name: "Вася",
//     age: 25
// };
// let petya = {
//     name: "Петя",
//     age: 30
// };
// let masha = {
//     name: "Маша",
//     age: 28
// };

// let users = [vasya, petya, masha];

// let names = []

// users.map(user => {
//     names.push(user.name)
// })

// console.log(names); // Вася, Петя, Маша

// 5
// let vasya = { name: "Вася", surname: "Пупкин", id: 1 };
// let petya = { name: "Петя", surname: "Иванов", id: 2 };
// let masha = { name: "Маша", surname: "Петрова", id: 3 };

// let users = [ vasya, petya, masha ];

// let usersMapped = users.map(user => ({
//     fullName: `${user.name} ${user.surname}`,
//     id: user.id
//   }));

//   alert( usersMapped[0].id ); // 1
//   alert( usersMapped[0].fullName );


// 6
// const sortByAge = (arr) =>{
//     arr.sort((a, b) => a.age > b.age ? 1 : -1);
// }

// let vasya = { name: "Вася", age: 25 };
// let petya = { name: "Петя", age: 30 };
// let masha = { name: "Маша", age: 28 };

// let arr = [ vasya, petya, masha ];

// sortByAge(arr);

// 7
// const shuffle = (arr) => {
//     arr.sort(() => Math.random() - 0.5)
// }

// let arr = [1, 2, 3];

// shuffle(arr)
// console.log(arr)

// shuffle(arr)
// console.log(arr)

// 8
// const getAverageAge = (arr) => {
//     let sum = 0
//     arr.map(obj => {
//         sum += obj.age
//     })
//     return sum / arr.length
// }

// let vasya = {
//     name: "Вася",
//     age: 25
// };
// let petya = {
//     name: "Петя",
//     age: 30
// };
// let masha = {
//     name: "Маша",
//     age: 29
// };

// let arr = [vasya, petya, masha];

// console.log(getAverageAge(arr)); // (25 + 30 + 29) / 3 = 28

// 9
const unique = (arr) => {
    let arrUnique = []
    arr.map((word, i) => {
        if (arr.indexOf(word, 0) == i && arr.indexOf(word, i + 1) == -1) {
            arrUnique.push(word)
        }
    })
    return arrUnique
}

let strings = ["кришна", "кришна", "харе", "харе",
    "харе", "харе", "кришна", "кришна", ":-O"
];

console.log(unique(strings)); // кришна, харе, :-O