const usFirst = (str) => {
    if (!str) {
        return str
    } else {
        return str[0].toUpperCase() + str.slice(1)
    }
}

const checkSpam = (str) =>{

    let lowerStr = str.toLowerCase()
    
    return lowerStr.includes('viagra') || lowerStr.includes('XXX')
}

const truncate = (str, maxlength) =>{
    if(str.length > maxlength){
        return str.substr(0, maxlength-1) + '...'
    }else{
        return str
    }
}

const extractCurrencyValue = (str) =>{
    return +str.slice(1)
}