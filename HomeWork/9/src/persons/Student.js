import {Person} from './Person'

export class Student extends Person {
    constructor(name, surname, birthDay, course) {
        super(name, surname);   
        this.birthDay = birthDay;
        this.course = course;
    }

    show() {
        super.show()
        console.log("тип: Student")
        console.log('Дата рождения: ' + this.birthDay)
        console.log('Курс: ' + this.course)
    }
}