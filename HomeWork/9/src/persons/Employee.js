import {Person} from './Person'

export class Employee extends Person {
    static #salary = 0;
    static #salaryPayments = [];

    constructor(name, surname, salary, salaryPayments) {
        super(name, surname)    
        Employee.#salary = salary;
        Employee.#salaryPayments = salaryPayments;
    }

    setSalary(newSalary){
        Employee.#salary = newSalary
    }

    getSalary(){
        return Employee.#salary
    }

    setSalaryPayments(){
        let date = new Date()
        Employee.#salaryPayments.push(date.toLocaleDateString() + ' получил ' + Employee.#salary + ' рублей')
    }

    getSalaryPayments(){
        return Employee.#salaryPayments
    }

    show() {
        super.show()
        console.log("Тип: Employee")
        console.log('Зарплата: ' + Employee.#salary)
        console.log('Полученные зараплаты:')
        console.log(Employee.#salaryPayments)
    }


}