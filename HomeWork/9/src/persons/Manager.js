import {Person} from './Person'
export class Manager extends Person {
    static #salary = 0;
    static #salaryPayments = 0;
    static #SubordinateEmployees = [];
    constructor(name, surname, salary, salaryPayments, SubordinateEmployees) {
        super(name, surname)
        Manager.#salary = salary;
        Manager.#salaryPayments = salaryPayments;
        Manager.#SubordinateEmployees = SubordinateEmployees;
    }

    setSalary(newSalary){
        Manager.#salary = newSalary
    }

    getSalary(){
        return Manager.#salary
    }

    setSalaryPayments(){
        let date = new Date()
        Manager.#salaryPayments.push(date.toLocaleDateString() + ' получил ' + Manager.#salary + ' рублей')
    }

    getSalaryPayments(){
        return Manager.#salaryPayments
    }

    getSubordinateEmployees(){
        return Manager.#SubordinateEmployees
    }

    takeOnJob(person) { //принять на работу
        if (!person) {
            throw new Error('Пустое поле для ввода')
        }
        Manager.#SubordinateEmployees.push(person)
    }

    dismissEmployee(employee) { //уволить сотрудника
        let idEmployee = Manager.#SubordinateEmployees.indexOf(employee);
        if (idEmployee == -1) {
            throw new Error('Сотрудник не существует')
        }
        Manager.#SubordinateEmployees.splice(idEmployee, 1)
    }

    show() {
        super.show()
        console.log("Должность: Manager")
        console.log('Список подчинённых:')
        console.log(Manager.#SubordinateEmployees)
    }
}