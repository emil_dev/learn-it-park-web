import {Person} from './persons/Person'
import {Employee} from './persons/Employee'
import {Manager} from './persons/Manager'
import {Student} from './persons/Student'

let salaryPaymentsIbraimov = ['10 января получил 20 000 рублей', '10 февраля получил 20 000 рублей', ]
let salaryPaymentsIvanov = ['10 января получил 20 000 рублей', '10 февраля получил 20 000 рублей', ]
let SubordinateEmployees = ['Ibraimov Emil', 'Petrov Ivan', 'Ivanov Petr' ]
let person = new Person ('name', 'surname');
let Petrov = new Student ('Petrov', 'Denis', "02.04.2002", 2);
let Ibraimov = new Employee ('Ibraimov', 'Emil', 1000000, salaryPaymentsIbraimov);
let Ivanov = new Manager ('Ivanov', 'Ivan', 35000, salaryPaymentsIvanov, SubordinateEmployees);
person.show();
Petrov.show();
Ibraimov.show();
Ivanov.show();

Ivanov.setSalary(120000) //изменена зарплата Иванова

Ibraimov.setSalaryPayments() //начислена зарплата
Ivanov.setSalaryPayments() //начислена зарплата

Ivanov.dismissEmployee('Petrov Ivan') //уволен сотрудник
Ivanov.takeOnJob('Novikov Kirill') //принят сотрудник

console.log(Ivanov.getSubordinateEmployees()); //выводим список сотрудников 

console.log(Ibraimov.getSalary()) //вывод зарпалаты

console.log(Ibraimov.getSalaryPayments()) //вывод cписка зарпалат

