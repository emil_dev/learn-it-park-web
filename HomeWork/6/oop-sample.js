class Person {
    constructor(name, surname) {    
        this.surname = surname;
        this.name = name;
    }
    
    show() {
        console.log('class Person')
        console.log(this.name)
        console.log(this.surname)
    }
    
}

class Student extends Person {
    constructor(name, surname, birthDay, kurs) {
        super(name, surname);   
        this.birthDay = birthDay;
        this.kurs = kurs;
    }

    show() {
        console.log('class Student')
        super.show()
        console.log(this.birthDay)
        console.log(this.kurs)
    }
}

class Employee extends Person {
    constructor(name, surname, salary, salaryPayments) {
        super(name, surname)    
        this.surname = surname;
        this.name = name;
        this.salary = salary;
        this.salaryPayments = salaryPayments;
    }

    show() {
        console.log("class Employee")
        super.show()
        console.log(this.salary)
        console.log(this.salaryPayments)
    }
}

class Manager extends Person {
    constructor(name, surname, salary, salaryPayments, SubordinateEmployees) {
        super(name, surname)    
        super.show()
        this.salary = salary;
        this.salaryPayments = salaryPayments;
        this.SubordinateEmployees = SubordinateEmployees;
    }

    show() {
        console.log("class Manager")
        super.show()
        console.log(this.salary)
        console.log(this.salaryPayments)
        console.log(this.SubordinateEmployees)
    }
}

let salaryPayments = ['10 января получил 20 000', '10 февраля получил 30 000', ]
let SubordinateEmployees = ['Ibraimov Emil', 'Petrov Ivan', 'Ivanov Petr' ]
let person = new Person ('name', 'surname');
let Petrov = new Student ('Petrov', 'Denis', "02.04.2002", 2);
let Ibraimov = new Employee ('Ibraimov', 'Emil', 1000000, salaryPayments);
let Ivanov = new Manager ('Ivanov', 'Ivan', 35000, salaryPayments, SubordinateEmployees);
person.show();
Petrov.show();
Ibraimov.show();
Ivanov.show();

