
class Content {
    constructor(name) {    
        this.name = name;
    }

    show() {
        console.log(this.name)
    }
}

let content = new Content ('sad');
content.show();